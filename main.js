/* global tmImage, Image, localStorage */

import * as gradCam from './gradCam.js'

// More API functions here:
// https://github.com/googlecreativelab/teachablemachine-community/tree/master/libraries/image

// the link to your model provided by Teachable Machine export panel
// const URL = 'https://teachablemachine.withgoogle.com/models/Se2BsLUiB/'
const url = './modelVS/'

let model, webcam, labelContainer, pause

init(url)

const startControl = document.querySelector('#capture')
startControl.addEventListener('click', function (e) {
  pause = false
  webcam.play()
  window.requestAnimationFrame(loop)
})

const stopControl = document.querySelector('#stop')
stopControl.addEventListener('click', function (e) {
  pause = true
  webcam.pause()
})

// localStorage
function setStorage (setPalette, setOpacity) {
  const paramStorage = {
    palette: setPalette,
    opacity: setOpacity
  }
  const objetStorage = JSON.stringify(paramStorage)
  localStorage.setItem('obj', objetStorage)
}

function getParametersFromStorage () {
  if (localStorage.getItem('obj')) {
    const objJson = JSON.parse(localStorage.getItem('obj'))
    const opacity = objJson.opacity
    const palette = objJson.palette
    return { palette, opacity }
  }
}

// Load the image model and setup the webcam
async function init (url) {
  const modelURL = url + 'model.json'
  const metadataURL = url + 'metadata.json'

  // load the model and metadata
  model = await tmImage.load(modelURL, metadataURL)
  // Convenience function to setup a webcam
  const flip = true // whether to flip the webcam
  webcam = new tmImage.Webcam(224, 224, flip) // width, height, flip
  await webcam.setup() // request access to the webcam
  await webcam.play()

  // setting localStorage
  if ((!getParametersFromStorage()) || (!getParametersFromStorage().opacity) || (!getParametersFromStorage().palette)) {
    setStorage('viridis', '0.5')
  }

  // syncronize the parametres from localStorage and Dom
  // opacity
  document.querySelector('#myRange').value = getParametersFromStorage().opacity

  document.querySelector('#myRange').addEventListener('click', function (e) {
    const opacitySelect = document.querySelector('input#myRange').value
    setStorage(getParametersFromStorage().palette, opacitySelect)
  })
  document.querySelector('#myRange').addEventListener('touchend', function (e) {
    const opacitySelect = document.querySelector('input#myRange').value
    setStorage(getParametersFromStorage().palette, opacitySelect)
  })
  // palette
  const palette = document.querySelector('select')
  const elts = palette.querySelectorAll('option')
  palette.value = getParametersFromStorage().palette

  palette.addEventListener('click', function (e) {
    let i
    for (i = 0; i < elts.length; i++) {
      if (elts[i].selected === true) break
    }
    const setPalette = elts[i].value
    setStorage(setPalette, getParametersFromStorage().opacity)
  })

  // loop
  window.requestAnimationFrame(loop)
  // append elements to the DOM
  document.getElementById('webcam-container').appendChild(webcam.canvas)

  labelContainer = document.getElementById('label-container')
  for (let i = 0; i < model.getTotalClasses(); i++) { // and class labels
    labelContainer.appendChild(document.createElement('div'))
  }
}

async function loop () {
  if (!pause) {
    webcam.update()
    await predict()
    window.requestAnimationFrame(loop)
    await drawGradCam()
    console.log(tf.memory().numBytesInGPU)
  }
}

async function copyAsCanvas (imageSource) {
  const canvas = document.createElement('canvas')
  canvas.width = imageSource.clientWidth
  canvas.height = imageSource.clientHeight
  canvas.getContext('2d').drawImage(imageSource, 0, 0, canvas.width, canvas.height)
  // const img = new Image(canvas.width, canvas.height)
  // img.src = canvas.toDataURL()
  return canvas
}

async function predict () {
  // predict can take in an image, video or canvas html element
  const prediction = await model.predict(webcam.canvas)
  for (let i = 0; i < model.getTotalClasses(); i++) {
    const classPrediction =
      prediction[i].className + ': ' + prediction[i].probability.toFixed(2)
    labelContainer.childNodes[i].innerHTML = classPrediction
  }
}

async function drawGradCam () {
  const cat = document.querySelector('#category-select').value
  const input = await copyAsCanvas(webcam.canvas)
  const destinationCanvas = document.getElementById('myCanvasTensor')
  destinationCanvas.width = webcam.canvas.width
  destinationCanvas.height = webcam.canvas.height
  const parameters = getParametersFromStorage()
  gradCam.drawInCanvas(input, model, destinationCanvas, parameters, cat)
}
