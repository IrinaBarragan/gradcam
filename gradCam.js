/* global tf */

// capture, cropTp and cropTensor are the function from customMobilNet (from Teachable Machine) use for prediction of one image

/* eslint-disable */

function capture(image) {

  return tf.tidy(() => {
    const pixels = tf.browser.fromPixels(image)
    // crop the image so we're using the center square
    const cropped = cropTensor(pixels, false, false)

    // Expand the outer most dimension so we have a batch size of 1
    const batchedImage = cropped.expandDims(0)
    pixels.dispose()
    cropped.dispose()
    // Normalize the image between -1 and a1. The image comes in between 0-255
    // so we divide by 127 and subtract 1.
    return batchedImage.toFloat().div(tf.scalar(127)).sub(tf.scalar(1))
  })
}

function cropTo(image, size, flipped) {
  let canvas = document.createElement('canvas')
  // image image, bitmap, or canvas
  let width = image.width
  let height = image.height
  const min = Math.min(width, height)
  const scale = size / min
  const scaledW = Math.ceil(width * scale)
  const scaledH = Math.ceil(height * scale)
  const dx = scaledW - size
  const dy = scaledH - size
  canvas.width = canvas.height = size
  let ctx = canvas.getContext('2d')
  ctx.drawImage(image, ~~(dx / 2) * -1, ~~(dy / 2) * -1, scaledW, scaledH)
  // canvas is already sized and cropped to center correctly
  if (flipped) {
    ctx.scale(-1, 1)
    ctx.drawImage(canvas, size * -1, 0)
  }
  return canvas
}

function cropTensor(img, grayscaleModel, grayscaleInput) {
  const size = Math.min(img.shape[0], img.shape[1])
  const centerHeight = img.shape[0] / 2
  const beginHeight = centerHeight - (size / 2)
  const centerWidth = img.shape[1] / 2
  const beginWidth = centerWidth - (size / 2)

  if (grayscaleModel && !grayscaleInput) {
    //cropped rgb data
    let grayscale_cropped = img.slice([beginHeight, beginWidth, 0], [size, size, 3])
    grayscale_cropped = grayscale_cropped.reshape([size * size, 1, 3])
    const rgb_weights = [0.2989, 0.5870, 0.1140]
    grayscale_cropped = tf.mul(grayscale_cropped, rgb_weights)
    grayscale_cropped = grayscale_cropped.reshape([size, size, 3])
    grayscale_cropped = tf.sum(grayscale_cropped, -1)
    grayscale_cropped = tf.expandDims(grayscale_cropped, -1)
    // return grayscale_cropped
  }
  return img.slice([beginHeight, beginWidth, 0], [size, size, 3])
}



/* eslint-enable */
export function drawInCanvas (image, model, destinationCanvas, parameters, cat) {
  const imageTensor = tf.tidy(() => {
    const croppedImage = cropTo(image, destinationCanvas.width, false)
    const captured = capture(croppedImage, false)
    return captured
  })

  // array for activation cards
  const activationCards = []
  const layersWeights = []

  //  push first layer - input
  // get activations cards
  activationCards.push(imageTensor)
  tf.tidy(() => {
    let layer
    layersWeights.push(model.model.layers[0].layers[0].layers[0].getWeights())
    for (let i = 1; i <= 27; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 27) {
        layer = model.model.layers[0].layers[0].layers[27].apply([activationCards[26], activationCards[18]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 28; i <= 45; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 45) {
        layer = model.model.layers[0].layers[0].layers[45].apply([activationCards[44], activationCards[36]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 46; i <= 54; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 54) {
        layer = model.model.layers[0].layers[0].layers[54].apply([activationCards[53], activationCards[45]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 55; i <= 72; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 72) {
        layer = model.model.layers[0].layers[0].layers[72].apply([activationCards[71], activationCards[63]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 73; i <= 81; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 81) {
        layer = model.model.layers[0].layers[0].layers[81].apply([activationCards[80], activationCards[72]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 82; i <= 90; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 90) {
        layer = model.model.layers[0].layers[0].layers[90].apply([activationCards[89], activationCards[81]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 91; i <= 107; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 107) {
        layer = model.model.layers[0].layers[0].layers[107].apply([activationCards[106], activationCards[98]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }

      activationCards.push(layer)
    }

    for (let i = 108; i <= 116; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 116) {
        layer = model.model.layers[0].layers[0].layers[116].apply([activationCards[115], activationCards[107]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 117; i <= 134; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 134) {
        layer = model.model.layers[0].layers[0].layers[134].apply([activationCards[133], activationCards[125]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 135; i <= 143; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      if (i === 143) {
        layer = model.model.layers[0].layers[0].layers[143].apply([activationCards[134], activationCards[142]])
      } else {
        layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      }
      activationCards.push(layer)
    }

    for (let i = 144; i <= 154; i++) {
      layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
      layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
      activationCards.push(layer)
    }

    layer = model.model.layers[0].layers[1]
    layersWeights.push(layer.getWeights())
    activationCards.push(layer.apply(activationCards[154]))

    layer = model.model.layers[1].layers[0]
    layersWeights.push(layer.getWeights())
    activationCards.push(layer.apply(activationCards[155]))

    layer = model.model.layers[1].layers[1]
    layersWeights.push(layer.getWeights())
    activationCards.push(layer.apply(activationCards[156]))

    // ______________ winning category
    // prediction result
    const result = activationCards[157].dataSync()
    // index of winning category
    let indexWin = cat
    if(cat==result.length+1){
      indexWin = result.indexOf(Math.max(...result))
    }
    // weights of last layer (categorie layer)
    const weights157 = layersWeights[157]
    // weights of layer caterories size 100 * number of categories
    const weightslayer157 = tf.tidy(() => {
      return tf.unstack(weights157[0].reshape([weights157[0].shape[0], weights157[0].shape[1]]), 1)
    })
    // weigths of winning category
    const weigthsWin = weightslayer157[indexWin].dataSync()

    for (let n = 0; n < weightslayer157.length; n++) {
      weightslayer157[n].dispose()
    }

    // __________last convolutional layer before GAP (Global Average Pooling)
    const lastConvLayer = activationCards[154]
    const layer156 = model.model.layers[1].layers[0]
    const layerCAM = layer156.apply(lastConvLayer)

    // 100 activation cards 7x7
    const layerList156 = tf.tidy(() => {
      return tf.unstack(layerCAM.reshape([layerCAM.shape[1], layerCAM.shape[2], layerCAM.shape[3]]), 2)
    })

    // ___________last layer multipled with winning category weights
    let sumWin = layerList156[0].mul(weigthsWin[0])
    for (let n = 1; n < layerList156.length; n++) {
      const a = layerList156[n].mul(weigthsWin[n])
      sumWin = a.add(sumWin)
    }

    layerCAM.dispose()
    for (let n = 0; n < layerList156.length; n++) {
      layerList156[n].dispose()
    }
    // reshape to 224x224
    sumWin = sumWin.reshape([7, 7, 1])
    const winResized = tf.image.resizeBilinear(sumWin, [224, 224])
    sumWin.dispose()

    // __________draw
    // ____________putImageData

    function convertToRGBImage (data, imgData, parameters) {
      const maxData = Math.max(...data.dataSync())
      const minData = Math.min(...data.dataSync())
      const interval = maxData - minData
      let j = 0

      const opacity = parameters.opacity
      const palette = parameters.palette

      for (let i = 0; i < data.dataSync().length; i++) {
        const colorPixel = data.dataSync()[i]

        const rgb = evaluate_cmap((colorPixel - minData) / (maxData - minData), palette, false)

        imgData.data[j + 0] = rgb[0]
        imgData.data[j + 1] = rgb[1]
        imgData.data[j + 2] = rgb[2]
        if (colorPixel >= maxData - interval * 0.2) {
          if (opacity === '0') {
            imgData.data[j + 3] = 180
          } else {
            imgData.data[j + 3] = 240
          }
        } else if (maxData - interval * 0.2 > colorPixel >= maxData - interval * 0.5) {
          imgData.data[j + 3] = opacity * 255
        } else if (maxData - interval * 0.5 > colorPixel >= maxData - interval * 0.8) {
          imgData.data[j + 3] = (opacity) * 255
        } else {
          imgData.data[j + 3] = opacity * 240
        }
        j = j + 4
      }
      for (let n = 0; n < data.length; n++) {
        data[n].dispose()
      }
      return imgData
    }

    const context = destinationCanvas.getContext('2d')
    const imgData = context.createImageData(destinationCanvas.width, destinationCanvas.height)
    convertToRGBImage(winResized, imgData, parameters)
    context.putImageData(imgData, 0, 0)

    for (let n = 0; n < activationCards.length; n++) {
      activationCards[n].dispose()
    }
  })
}
